# Usage

You should extend this docker image to use it, an example can be found [here](https://bitbucket.org/gcaillaut/docker-hello-shiny).

Shiny applications must be located in `/home/shiny/apps`. Make sure that the shiny user owns the directories containing the shiny applications.

Then you can run the docker image with:

```shell
docker run -p 3838:3838 your-docker-image
```
