FROM ubuntu:16.04

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "--yes", "r-base", "gdebi-core", "wget", "git"]

RUN ["wget", "https://download3.rstudio.org/ubuntu-12.04/x86_64/shiny-server-1.5.6.875-amd64.deb"]
RUN ["gdebi", "-n", "shiny-server-1.5.6.875-amd64.deb"]
RUN ["R", "-e", "install.packages(c('shiny', 'rmarkdown'), repos = 'https://cloud.r-project.org')"]

EXPOSE 3838

COPY shiny-server.conf /etc/shiny-server/shiny-server.conf

USER shiny:shiny
WORKDIR /home/shiny

RUN ["mkdir", "/home/shiny/apps", "/home/shiny/log", "/home/shiny/bookmarks"]

ENTRYPOINT ["shiny-server"]
